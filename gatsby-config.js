module.exports = {
  siteMetadata: {
    title: 'Woodwork By Hand',
    shortTitle: 'By Hand',
    description: 'My journey through absolute novice woodworking',
  },
  plugins: [
    'gatsby-plugin-emotion',
    'gatsby-plugin-react-svg',
    'gatsby-plugin-typescript',
    'gatsby-plugin-typescript-checker',
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    'gatsby-plugin-image',
    'gatsby-remark-images',
    {
      resolve: 'gatsby-plugin-mdx',
      options: {
        gatsbyRemarkPlugins: [{
          resolve: 'gatsby-remark-images',
          options: {
            maxWidth: 800,
            showCaptions: true,
          }
        }]
      }
    },
    {
      resolve: 'gatsby-plugin-typegen',
      options: {
        outputPath: 'src/generated/graphql-types.d.ts',
        emitSchema: { 'src/generated/gatsby-introspection.json': true },
        emitPluginDocuments: { 'src/generated/gatsby-plugin-documents.graphql': true },
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'posts',
        path: `${process.cwd()}/posts`,
      },
    },
  ],
};