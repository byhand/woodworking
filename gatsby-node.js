const path = require('path');
const { createFilePath } = require('gatsby-source-filesystem');

module.exports = {
  onCreateNode: ({ node, getNode, actions: { createNodeField } }) => {
    if (node.internal.type === 'Mdx') {
      const value = createFilePath({ node, getNode });
      createNodeField({
        node,
        value,
        name: 'slug',
      });
    }
  },
  createPages: async ({ graphql, actions: { createPage }, reporter }) => {
    const result = await graphql(`
      query AllPages {
        allMdx {
          nodes {
            id
            slug
            frontmatter {
              categories
            }
          }
        }
      }
    `);

    if (result.errors) {
      reporter.panicOnBuild('ERROR: Loading "createPages" query');
    }

    const categories = {};
    const dates = {};

    const posts = result.data.allMdx.nodes;
    for (const node of posts) {
      for (const category of node.frontmatter.categories || []) {
        categories[category] = (categories[category] || 0) + 1;
      }
      const [year, month] = node.slug.split('/');
      dates[year] = dates[year] || {};
      dates[year][month] = (dates[year][month] || 0) + 1;

      createPage({
        component: path.resolve('./src/templates/post.tsx'),
        path: node.slug,
        context: { id: node.id },
      });
    }

    for (const category in categories) {
      createPage({
        component: path.resolve('./src/templates/by-category.tsx'),
        path: category,
        context: { category },
      });
    }

    for (const year in dates) {
      const nextYear = (parseInt(year, 10) + 1).toString().padStart(4, '0');
      createPage({
        component: path.resolve('./src/templates/by-date.tsx'),
        path: year,
        context: { year, gte: year, lt: nextYear },
      });
      for (const month in dates[year]) {
        const nextMonth = (parseInt(month, 10) + 1).toString().padStart(2, '0');
        createPage({
          component: path.resolve('./src/templates/by-date.tsx'),
          path: `${year}/${month}`,
          context: {
            year,
            month,
            gte: `${year}-${month}`,
            lt: `${year}-${nextMonth}`,
          },
        });
      }
    }
  },
};
