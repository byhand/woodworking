module.exports = {
  schema: ["src/generated/gatsby-introspection.json"],
  documents: ["src/generated/gatsby-plugin-documents.graphql"],
  extensions: {
    endpoints: {
      default: {
        url: "http://leia.local:8000/___graphql",
        headers: { "user-agent": "JS Graphql" },
        introspect: false
      }
    }
  }
}