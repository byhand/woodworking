---
title: Side Gate
date: 2021-05-02
featuredImage: side-gate/figure13.jpg
categories:
  - projects
published: true
---

# The Purpose

Alright, it's first project time! Having moved into our new place (complete with space for full workshop) we're going through the process of making a half-acre safe for the dogs. As a first step we put in some temporary fencing around the yard area, including resting an old metal gate against the wall which can be 'opened' when needed. After a few months of having to open this gate, I decided we needed a real solution to this so I started looking at the options. As we wandered around our neighbourhood, we found a few gate styles we liked and noticed they mostly came from a single company - [Country Gates](https://countrygates.com.au/).

![Full Width 'Country Gates' Gate](side-gate/figure1.jpg)

So I had a look at the country gates price list and we'd be looking at at least $1000 to buy one, so it was a pretty significant purchase and I was wondering what other options we had. The next problem came when I started measuring the space where the gate needed to fit. In general, the gates I was looking at were either 900mm or 1200mm wide and the space I was trying to fill was over 1400mm. On top of this, I definitely wanted to be able to open the gate the whole way so we'd be able to bring bulky items like wheelbarrows through the gap. How much more was a custom gate going to cost!

![Gate Placement Measurements](side-gate/figure2.jpg)

# The Design

With the measurements in place, the dimensions became even more frustrating because a full width gate (1400mm) wouldn't be able to open fully - there was only space for a 1000mm gate at most. On the other hand, a double gate would only be 700mm on each side, which feels a little too small when measured out. After a little more playing around, I happened upon a solution that met all my criteria - a split gate where the 'small' side is generally locked in place with a drop bolt and the 'big' side is latched to the 'small' side. With that in mind, I started to dabble in some 3D modelling to get a feel for what looked good and, after a few iterations on my own, brought Kay in for some feedback and further iterations.

![3D Modelled Gate Design](side-gate/figure3.jpg)

With the 3D design in, I started drawing up my dimensions and confirming my material requirements, but I wanted to do one last investigation before I dove into it. I wasn't sure if I truly needed to but since it's my first big project, I thought it was better safe than sorry. So I cut the gate dimensions into a piece of cardboard and held it in the space and I was very glad I did. Seeing it in situ helped me to realise 2 things - firstly that my original design was too short and the gate needed to be 1000mm tall and secondly that the ratio was off. Where I had previously decided on a 1000mm/400mm split between the 2 sides, it would look more aesthetically pleasing if it were 900mm/500mm. This was fine since the average single gate is 900mm.

# The Build

I had my design, I had my materials and I had some time on my hands - I was ready to go!

## Step 1 - Cutting down the stock

I'd purchased 3 2.4m x 90mm x 90mm pieces treated pine posts for the frame of the gate and 4 2.4m _ 90mm _ 19mm pieces of dressed pine for the bars and diagonals. So I pulled out the mitre saw to cut it all down to size. Thankfully the process for this was simple and I'd already calculated my cuts to get the most value from the original stock. The mitre saw made short work of the task and I soon had all my pieces in the right size.

Since I had the mitre saw out already, I got a little ahead of myself and made some cuts that were purely for decoration. I wanted to get the effect that I'd seen on a number of gates where the top of the bollard formed a truncated pyramid. So I set the mitre saw to 45 degrees and lined the base of the cut 15mm from the top. I'd take a cut and then rotate the piece in the mitre saw 90 degrees till I had made the same cut on all 4 sides. I then repeated this on all 4 of the vertical frame pieces. The end result was fairly accurate and looked quite nice.

![Everything cut to size](side-gate/figure4.jpg)

## Step 2 - Fitting the frame

Before I could make accurate measurements for my bars or diagonals, I needed to fit the frame together. However, I had planned to screw the horizontal pieces in from the outside edge to increase the strength of the gate. I wanted a way to fit the whole piece together without permanently screwing it in, so I settled for a simple single dowel join. Down the track, I'd secure the dowels with glue and add the screws but, for now, a single dowel placed in the centre allowed me to join the pieces together to get a feel for their size and take measurements but still be able to pull them apart for cutting the mortises and making adjustments to the pieces.

To start with, I measured out the distance of the pieces from the end and marked either edge and the center, then used my new [centre marking gauge](/2021/05/centre-gauge) to find the centre of the post and drilled an 8mm hole where the centre of the bar should be. I repeated that process for the top and bottom of every vertical for a total of 8 holes. Then I marked the centre of the ends of all my horizontal pieces and drilled another 8 8mm holes.

![Prepping the dowel joins](side-gate/figure5.jpg)

With all of those holes measured and drilled, I grabbed myself some 8mm dowels and dry-fitted the gates together. For a brief period of time, I stood them on the bench and was happy with the general feel of the size and shape.

![Dry-fitting the frames](side-gate/figure6.jpg)

Now that the frames are ready it's time to move onto the internal bars and the requisite mortise and tenon joins. First things first, I wanted to make sure my measurements felt right once they were off the page so I took my small gate frame and rested the bars on top. Since the gap was 670mm, the distribution actually comes to some nicely round numbers. 3 90mm bars makes 270mm with 4 100mm gaps around them.

![Distributing the internal bars](side-gate/figure7.jpg)

## Step 3 - Fitting the bars

Since this was the first time I'd used a router, let alone cut a mortise and tenon join, I was very careful with the process. First used my centre gauge to mark the centre of the post and measured out the positions of the bars relative to the base of the post. I almost stopped there but realised I needed my joins to be smaller than the 90mm bars themselves.

After a little bit of research, I found some ['rules' for sizing tenons](https://www.woodmagazine.com/woodworking-how-to/joinery/mortise-tenon-joinery/rule-of-thumb-for-mortise-and-tenon-size) which I followed to get the right dimensions.

- _Thickness_: should be 1/3 of the 19mm rail, which is helpfully remarkably close to 1/4 inch (the diameter of my router bit), so I settled at _1/4 inch_.
- _Length_: should be 5 times the thickness. Rather than using the imperial measurement, I settled on 6mm. Multiplying that by 5 makes _30mm_ which coincidentally is 1/3 of the post.
- _Width_: should be 2/3 the width of the 90mm rail which makes _60mm_ - another nice round number.

With that sorted, I measured and marked my mortises ready for routing.

![Ready for routing](side-gate/figure8.jpg)

With that 1/4 inch router bit fitted, I would only have to make single horizontal pass to cut the mortice, but I needed a depth of 30mm. Since the height of the cutting area of the bit was only 15mm, I decided to play it safe and make 3 10mm passes for each mortise. I fitted the edge guide that was packaged with the plunge base for the router and very carefully set the bit so it was placed right over the centre line I'd marked previously.

Furthermore, since I wanted to be as accurate as possible, I clamped a couple of pieces of scrap to the post to restrict how far the router could travel along that centre line. This meant that cutting the mortice would be a matter of going back and forth between the two clamped pieces, while holding the edge guide flush against the edge and using the depth gauge to make each pass at the right height.

![Routing the mortices](side-gate/figure9.jpg)

Since the router was setup to cut the mortices, I repeated those steps on each of the posts and started preparing for the tenons. I'd read a few guides to cutting tenons with a router, so my first step was to build a jig for holding and positioning the tenon. I started with [Glen Huey's square platform jig](https://www.youtube.com/watch?v=TbvfxBkaexE) but through some combination of misunderstanding and misaligning, this just wasn't working for me.

After a little bit more research and experimentation, I settled on once again making use of the edge guide, rested against the main work piece as well as two scrap pieces of the same stock so I'm not worrying about the corners. Then it's just a matter of setting the plunge router to the right height and working my way across the both sides of the piece.

![Routing the tenons](side-gate/figure10.jpg)

The tenons fit the mortices nicely, with a few taps from the mallet to get them all the way in. I then drilled pilot holes on the outside edge of the posts so I could drive bolts all the way through to the cross-beams. With those bolts in place, I was ready to make the diagonals.

![Gates with fitted bars](side-gate/figure11.jpg)

## Step 4 - Fitting the diagonals

I wanted to fit the diagonals as accurately as possible so I wanted to find a way to place the exact centre of the board against the apex of the corner. As usual, the first step was to mark the centre of the board (that marking gauge was paying for itself) and then hammmer a small nail along that centre point.

Next, I placed the board against the frame to roughly determine the other corner and marked the centre line around that area. From there, I slid the board beneath the frame, slid the nail flush against the corner of the frame and aligned the centre line with the opposite corner. Finally, I marked the actual cut lines against the frame.

![Marking the diagonal board cuts](side-gate/figure12.jpg)

I rolled out the mitre saw again to get the perfect angle on the cuts. Once the initial cuts were made, I did have to make some slight alterations, but steady small increments meant I was able to dial in the dimensions resulting in a tight fit.

Last but not least, I used the mallet to set the boards flush against the bars.

![Gates with fitted diagonals](side-gate/figure13.jpg)

## Step 5 - Tidying Up

Before I was finished, I needed to fill a few gaps in the treated pine, and sand the entire gate to a smooth finish ready for painting.