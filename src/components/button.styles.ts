import styled from '@emotion/styled';
import { Link } from 'gatsby';

export const Cell = styled(Link)`
  border: 1px solid ${props => props.theme.colour.accentDark};
  color: ${props => props.theme.colour.accentDark};
  display: block;
  font-family: ${props => props.theme.font.action};
  font-weight: 400;
  font-size: 1.6rem;
  letter-spacing: 1px;
  text-decoration: none;
  margin: 3rem auto;
  padding: 0.8rem 3rem;
  width: fit-content;
`;