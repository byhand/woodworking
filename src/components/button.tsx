import * as React from 'react';

import { Cell } from './button.styles';

export type ButtonProps = {
  text: string;
  to: string;
};

export const Button = ({ text, to }: ButtonProps) => (
  <Cell to={to}>
    {text}
  </Cell>
);
