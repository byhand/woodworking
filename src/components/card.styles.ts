import styled from '@emotion/styled';
import { Link } from 'gatsby';

export const Cell = styled.article`
  display: block;
  position: relative;
  padding: 1rem;
  margin: 0.5rem 0;
  width: 100%;
`;

export const Detail = styled.div`
  background: ${props => props.theme.colour.light};
  border-bottom: 1px dotted ${(props) => props.theme.colour.accentDark};
  padding: 1rem 2rem;
  position: relative;
  margin: -15% auto 0;
  width: 80%;
  min-height: 20rem;
  
  display: flex;
  flex-flow: column; 
`;

export const Title = styled.h2`
  color: ${props => props.theme.colour.accentDark};
  font-family: ${props => props.theme.font.title};
  font-size: 3.4rem;
  margin: 0.6rem 0;
  text-align: center;

  a {
    cursor: pointer;
    text-decoration: none;
  }
`;

export const Metadata = styled.h3`
  color: ${props => props.theme.colour.accentLight};
  font-family: ${props => props.theme.font.action};
  font-size: 1.6rem;
  font-weight: 600;
  margin: 0.2rem 0;
  text-align: center;
`;

export const Summary = styled.p`
  font-weight: 300;
  font-size: 1.8rem;
  line-height: 1.6;
  margin: 0;
  flex: 1;
`;

export const ReadMore = styled(Link)`
  display: block;
  margin: 0.6rem;

  color: ${props => props.theme.colour.accentDark};
  text-align: center;
  font-size: 2rem;
  font-weight: 600;
`;