import * as React from 'react';
import { Link } from 'gatsby';
import {
  GatsbyImage,
  StaticImage,
  IGatsbyImageData,
} from 'gatsby-plugin-image';

import {
  Cell,
  Detail,
  Summary,
  Title,
  Metadata,
  ReadMore,
} from './card.styles';

export type CardProps = {
  id: string;
  slug?: string;
  image?: IGatsbyImageData;
  title?: string;
  date?: string;
  categories?: readonly (string | undefined)[];
  excerpt?: string;
};

const imageRatio = {
  width: 1500,
  height: 600,
};

const categoryDisplay = ({ categories }: Pick<CardProps, 'categories'>) => {
  const actual = categories?.filter(Boolean);
  return actual?.length ? ` | ${actual.join(', ')}` : undefined;
};

const makeLink = (slug?: string) =>
  slug && slug.startsWith('/') ? slug : `/${slug}`;

export const Card = ({
  title,
  slug,
  excerpt,
  date,
  categories,
  image,
}: CardProps) => (
  <Cell>
    {image ? (
      <GatsbyImage alt={title || ''} image={image} {...imageRatio} />
    ) : (
      <StaticImage
        alt={title || ''}
        src="https://placeimg.com/1500/600/arch/grayscale"
        {...imageRatio}
      />
    )}
    <Detail>
      <Metadata>
        {date}
        {categoryDisplay({ categories })}
      </Metadata>
      <Title>
        <Link to={makeLink(slug)}>{title}</Link>
      </Title>
      <Summary>{excerpt}</Summary>
      <ReadMore to={makeLink(slug)}>Read More</ReadMore>
    </Detail>
  </Cell>
);

export const WrappedCard = ({
  frontmatter,
  ...post
}: GatsbyTypes.PostCardFragment) => (
  <Card
    key={post.id}
    {...post}
    {...frontmatter}
    image={frontmatter?.featuredImage?.childImageSharp?.gatsbyImageData}
  />
);
