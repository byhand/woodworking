import styled from '@emotion/styled';
import { Link } from 'gatsby';

import Plane from '../images/plane.svg';

export const Cell = styled.header`
  display: flex;
  flex-flow: row;

  border-bottom: 1px solid ${(props) => props.theme.colour.accentLight};
  margin: 0 0 0.5em 0;
  width: 100%;
`;

export const Logo = styled(Link)`
  display: flex;
  flex-flow: column;
  text-decoration: none;
`;

export const Icon = styled(Plane)`
  width: 120px;
  height: 60px;
  path {
    stroke: ${props => props.theme.colour.accentDark};
    stroke-width: 10;
  }
`;

export const Mark = styled.h4`
  color: ${props => props.theme.colour.accentDark};
  font-family: ${props => props.theme.font.title};
  font-size: 2em;
  text-align: center;
  margin: 0;
  padding: 0.4em 0;
`;