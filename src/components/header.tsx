import * as React from 'react';

import { useSiteMetadata } from '../hooks/site-metadata';
import { Cell, Icon, Logo, Mark } from './header.styles';

export const Header = () => {
  const meta = useSiteMetadata();

  return (
    <Cell>
      <Logo to="/">
        <Icon />
        <Mark>{meta?.shortTitle}</Mark>
      </Logo>
    </Cell>
  );
};
