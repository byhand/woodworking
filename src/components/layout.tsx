import * as React from 'react';
import { Global, ThemeProvider } from '@emotion/react';

import { theme, globalStyles } from '../styles';
import { Header } from './header';

export const Layout: React.FC = ({ children }) => (
  <ThemeProvider theme={theme}>
    <Global styles={globalStyles} />
    <Header />
    {children}
  </ThemeProvider>
);
