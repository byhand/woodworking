import * as React from 'react';
import styled from '@emotion/styled';
import { Link as GatsbyLink } from 'gatsby';

export const Heading = styled.h1`
  color: ${(props) => props.theme.colour.accentDark};
  font-family: ${(props) => props.theme.font.body};
  font-size: 2.8em;
  font-weight: 800;
  margin: 2.4rem 0 1.2rem;
`;

export const Subheading = styled.h2`
  font-size: 2.2rem;
  font-style: italic;
  font-weight: 800;
  margin: 2.4rem 0 1.2rem;
`;

export const Blockquote = styled.blockquote`
  margin: 0 0 0 6rem;
  p {
    font-weight: 600;
    font-size: 2.4rem;
  }
`;

export const UnorderedList = styled.ul`
  padding-left: 6rem;
  list-style-type: none;

  li:before {
    position: absolute;
    margin: -1px 0 0 -2rem;

    font-family: sans-serif;
    font-weight: 300;
    content: '\\2013';
  }
`;

export const ListItem = styled.li`
  font-size: 1.9rem;
  line-height: 1.6;
  margin: 1rem 0;
`;

export const Link = (
  props: React.DetailedHTMLProps<
    React.AnchorHTMLAttributes<HTMLAnchorElement>,
    HTMLAnchorElement
  >,
) => {
  if (!props.href || props.href.match(/^https?:/)) {
    const target = props.target || '_blank';
    return <a {...props} target={target} />;
  }

  return <GatsbyLink to={props.href}>{props.children}</GatsbyLink>;
};
