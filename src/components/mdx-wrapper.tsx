import * as React from 'react';
import { MDXProvider, MDXProviderComponentsProp } from '@mdx-js/react';
import { MDXRenderer, MDXRendererProps } from 'gatsby-plugin-mdx';

import {
  Blockquote,
  Heading,
  Link,
  ListItem,
  Subheading,
  UnorderedList,
} from './mdx-wrapper.styles';

const components: MDXProviderComponentsProp = {
  h1: Heading,
  h2: Subheading,
  blockquote: Blockquote,
  ul: UnorderedList,
  li: ListItem,
  a: Link,
};

export const MDXWrapper = (props: MDXRendererProps) => (
  <MDXProvider components={components}>
    <MDXRenderer {...props} />
  </MDXProvider>
);
