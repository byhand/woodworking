import styled from '@emotion/styled';

export const Heading = styled.h1`
  color: ${(props) => props.theme.colour.accentDark};
  font-family: ${(props) => props.theme.font.title};
  font-size: 5.4em;
  text-align: center;
  margin: 5rem auto;
  max-width: 75rem;
`;

export const Subheading = styled.h2`
  font-size: 2.6rem;
  font-style: italic;
  font-weight: 300;
  text-align: center;
`;