import * as React from 'react';

import { Layout, Button } from '.';
import { Heading, Subheading } from './not-found.styles';

export const NotFound = () => {
  return (
    <Layout>
      <Heading>Looks like this page is still under construction.</Heading>
      <Subheading>
        Not a lot to say here, but perhaps you'd like to head back to the home
        page?
      </Subheading>
      <Button to="/" text="Go Home" />
    </Layout>
  );
};
