import { graphql, useStaticQuery } from 'gatsby';

export const siteMetadataQuery = graphql`
  query SiteMetaData {
    site {
      siteMetadata {
        title
        shortTitle
        description
      }
    }
  }
`;

export const useSiteMetadata = () => {
  const { site } = useStaticQuery<GatsbyTypes.SiteMetaDataQuery>(siteMetadataQuery);

  return site?.siteMetadata;
}