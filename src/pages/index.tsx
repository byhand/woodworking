import * as React from 'react';
import { graphql, PageProps } from 'gatsby';

import { Layout, WrappedCard } from '../components';
import { Main } from '../styles';

export const query = graphql`
  query AllPosts {
    allMdx(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { published: { eq: true } } }
    ) {
      nodes {
        ...PostCard
      }
    }
  }

  fragment PostCard on Mdx {
    id
    slug
    excerpt(pruneLength: 600)
    frontmatter {
      title
      date(formatString: "MMM D, YYYY")
      categories
      featuredImage {
        childImageSharp {
          gatsbyImageData(
            width: 1500
            height: 600
            placeholder: BLURRED
            transformOptions: { cropFocus: CENTER }
          )
        }
      }
    }
  }
`;

const IndexPage = ({ data }: PageProps<GatsbyTypes.AllPostsQuery>) => {
  return (
    <Layout>
      <Main>{data.allMdx.nodes.map(WrappedCard)}</Main>
    </Layout>
  );
};

export default IndexPage;
