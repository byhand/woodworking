declare module '*.svg' {
  import React = require('react');
  const component: React.FC<React.SVGProps<SVGSVGElement>>;
  export default component;
}
