import { css } from '@emotion/react';
import styled from '@emotion/styled';

import { theme } from './theme';

export const globalStyles = css`
  html {
    box-sizing: border-box;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto',
      'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans',
      'Helvetica Neue', sans-serif;
    font-size: 62.5%;
  }

  *,
  *:before,
  *:after {
    box-sizing: inherit;
    font-family: inherit;
    color: inherit;
  }

  body {
    color: ${theme.colour.dark};
    background-color: ${theme.colour.light};
    margin: 0;
    padding: 10px;
    font-family: ${theme.font.body};
  }

  p {
    font-size: 1.9rem;
    line-height: 1.8;
    margin: 1rem 0;
  }

  a {
    color: ${theme.colour.accentLight};
  }
`;

export const Main = styled.section`
  width: 98%;
  max-width: 120rem;
  margin: 0 auto;
`;

export const Heading = styled.h1`
  color: ${(props) => props.theme.colour.accentDark};
  font-family: ${(props) => props.theme.font.title};
  font-size: 5.4em;
  text-align: center;
  margin: 5rem auto;
  max-width: 75rem;
`;