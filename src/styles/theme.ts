import { Theme } from '@emotion/react';

export const white = '#E3DCD2';
export const lightGreen = '#578C56';
export const darkGreen = '#025946';
export const black = '#100C0D';

declare module '@emotion/react' {
  export interface Theme {
    colour: {
      dark: string;
      accentDark: string;
      light: string;
      accentLight: string;
    };
    font: {
      title: string;
      body: string;
      action: string;
    };
  }
}

export const theme: Theme = {
  colour: {
    dark: black,
    accentDark: darkGreen,
    light: white,
    accentLight: lightGreen,
  },
  font: {
    title: "Marcellus, Palatino, Georgia, serif",
    body: "'Cormorant Garamond', serif",
    action: 'Cinzel',
  }
};
