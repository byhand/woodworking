import * as React from 'react';
import { graphql, PageProps } from 'gatsby';

import { Layout, WrappedCard } from '../components';
import { Main, Heading, titleCase } from '../styles';

export const query = graphql`
  query ByCategory($category: String) {
    allMdx(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: {
        frontmatter: {
          categories: { in: [$category] }
          published: { eq: true }
        }
      }
    ) {
      nodes {
        ...PostCard
      }
    }
  }
`;

const ByCategory = ({
  data,
  pageContext,
}: PageProps<GatsbyTypes.ByCategoryQuery, { category: string }>) => {
  return (
    <Layout>
      <Main>
        <Heading>{titleCase(pageContext.category)} Posts</Heading>
        {data.allMdx.nodes.map(WrappedCard)}
      </Main>
    </Layout>
  );
};

export default ByCategory;
