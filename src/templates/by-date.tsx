import * as React from 'react';
import { graphql, PageProps } from 'gatsby';

import { Layout, WrappedCard } from '../components';
import { Main, Heading } from '../styles';

export const query = graphql`
  query ByDate($gte: Date, $lt: Date) {
    allMdx(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: {
        frontmatter: { date: { gte: $gte, lt: $lt }, published: { eq: true } }
      }
    ) {
      nodes {
        ...PostCard
      }
    }
  }
`;

const ByDate = ({
  data,
  pageContext,
}: PageProps<GatsbyTypes.ByDateQuery, { year: string; month?: string }>) => {
  return (
    <Layout>
      <Main>
        <Heading>
          {pageContext.month} {pageContext.year}
        </Heading>
        {data.allMdx.nodes.map(WrappedCard)}
      </Main>
    </Layout>
  );
};

export default ByDate;
