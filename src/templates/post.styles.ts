import styled from '@emotion/styled';

export const Hero = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  min-height: 50rem;

  position: relative;
  margin-bottom: 3rem;
  width: 100%;
  overflow: hidden;
`;

export const Background = styled.div`
  position: absolute;
  top: -10px;
  left: -10px;
  right: -10px;
  bottom: -10px;
  filter: blur(15px) contrast(0.5) brightness(1.5);
`;

export const Detail = styled.div`
  position: relative;
`;

export const Title = styled.h1`
  color: ${props => props.theme.colour.accentDark};
  font-family: ${props => props.theme.font.title};
  font-size: 6em;
  text-align: center;
  margin: 1rem 0 1.6rem;
`;

export const Metadata = styled.h3`
  color: ${props => props.theme.colour.accentLight};
  font-family: ${props => props.theme.font.action};
  font-size: 1.8rem;
  font-weight: 600;
  margin: 1rem 0;
  text-align: center;

  a {
    font-size: 2.1rem;
    border-right: 1px solid ${props => props.theme.colour.accentLight};
    padding: 1rem;
    margin: 0;

    &:last-child {
      border-right: 0;
    }
  }
`;

export const Article = styled.article`
  .gatsby-resp-image-figure {
    margin: 0 auto;
    max-width: 50rem;
    padding: 1rem;
    text-align: center;

    .gatsby-resp-image-figcaption {
      font-size: 1.7rem;
      font-weight: 700;
    }
  }
`;