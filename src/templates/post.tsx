import * as React from 'react';
import { graphql, Link, PageProps } from 'gatsby';
import { GatsbyImage, StaticImage } from 'gatsby-plugin-image';

import { Layout, MDXWrapper, NotFound } from '../components';
import { Main } from '../styles';
import {
  Hero,
  Background,
  Detail,
  Title,
  Metadata,
  Article,
} from './post.styles';

export const query = graphql`
  query Post($id: String) {
    mdx(id: { eq: $id }) {
      id
      body
      wordCount {
        words
      }
      timeToRead
      frontmatter {
        title
        date(formatString: "MMM D YYYY")
        categories
        featuredImage {
          childImageSharp {
            gatsbyImageData(
              width: 1220
              height: 520
              placeholder: BLURRED
              transformOptions: { cropFocus: CENTER }
            )
          }
        }
      }
    }
  }
`;

const Post = ({ data: { mdx } }: PageProps<GatsbyTypes.PostQuery>) => {
  if (!mdx || !mdx.frontmatter) {
    return <NotFound />;
  }
  const {
    frontmatter: { title, categories, date, featuredImage },
    wordCount,
    timeToRead,
    body,
  } = mdx;
  const image = featuredImage?.childImageSharp?.gatsbyImageData;

  return (
    <Layout>
      <Main>
        <Hero>
          <Background>
            {image ? (
              <GatsbyImage alt={title || ''} image={image} />
            ) : (
              <StaticImage
                alt={title || ''}
                src="https://placeimg.com/1220/520/arch/sepia"
              />
            )}
          </Background>
          <Detail>
            <Metadata>
              {categories?.map((category) => (
                <Link to={`/${category}`}>{category}</Link>
              ))}
              <Link to="..">{date}</Link>
            </Metadata>
            <Title>{title}</Title>
            <Metadata>{wordCount?.words} words, {timeToRead} minutes</Metadata>
          </Detail>
        </Hero>
        <Article>
          <MDXWrapper>{body}</MDXWrapper>
        </Article>
      </Main>
    </Layout>
  );
};

export default Post;
